function git_get_default_branch
	git symbolic-ref refs/remotes/origin/HEAD | rev | cut -d/ -f1 | rev
end
