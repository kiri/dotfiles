if status is-interactive
    starship init fish | source
    zoxide init fish | source
    atuin init --disable-up-arrow fish | source
    set -gx EDITOR nvim
    abbr -a -- bi 'brew install'
    abbr -a -- bli 'brew list --installed-on-request'
    abbr -a -- bu 'brew uninstall'
    abbr -a -- cea 'chezmoi edit --apply'
    abbr -a -- cl 'curl -L'
    abbr -a -- clo 'curl -LO'
    abbr -a -- cp 'cp -i'
    abbr -a -- ct 'cd $(mktemp -d)'
    abbr -a -- fex 'fd --extension'
    abbr -a -- fl 'flatpak list'
    abbr -a -- fpi 'flatpak install'
    abbr -a -- fps 'flatpak search'
    abbr -a -- fs 'flatpak search'
    abbr -a -- fu 'flatpak uninstall --assumeyes'
    abbr -a -- g git
    abbr -a -- gb 'git branch'
    abbr -a -- gca 'git commit -a'
    abbr -a -- gcb 'git checkout -b'
    abbr -a -- gch 'git checkout'
    abbr -a -- gcm 'git checkout master || git checkout main'
    abbr -a -- gl 'git log --oneline --decorate --graph --all'
    abbr -a -- gr 'git revert'
    abbr -a -- grg 'git remote get-url origin'
    abbr -a -- gri 'git rebase --interactive'
    abbr -a -- k9 'kill -9'
    abbr -a -- l 'ls -l'
    abbr -a -- lg lazygit
    abbr -a -- ll 'ls -la'
    abbr -a -- md 'mkdir -p'
    abbr -a -- mv 'mv -i'
    abbr -a -- pll 'git pull'
    abbr -a -- psf 'git push --force-with-lease'
    abbr -a -- psff 'git push --force'
    abbr -a -- psh 'git push'
    abbr -a -- pum 'git pull upstream master'
    abbr -a -- pump 'git pull upstream master && git push'
    abbr -a -- pur 'git pull upstream master --rebase'
    abbr -a -- roi 'rpm-ostree install'
    abbr -a -- ros 'rpm-ostree status'
    abbr -a -- rou 'rpm-ostree upgrade'
    abbr -a -- sid setsid
    abbr -a -- ss show-script
end
