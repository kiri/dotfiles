{ pkgs, lib, ... }:
let
  gamescope = {
    enable = true;
    args = [
      "-H 1080"
      "-W 1920"
      "--adaptive-sync"
    ];
  };
in
{
  users.users.kiri.packages = lib.attrValues {
    inherit (pkgs)
      # heroic
      # TODO: Check why it's marked as broken
      # itch
      mangohud
      ;
    # inputs.nix-gaming.packages.${pkgs.system}.rocket-league
  };
  programs = {
    inherit gamescope;
    gamemode.enable = true;
    steam = {
      enable = true;
      gamescopeSession = gamescope;
      extraCompatPackages = [ pkgs.proton-ge-bin ];
    };
  };
  hardware = {
    graphics = {
      enable32Bit = true;
      extraPackages32 = [ pkgs.pkgsi686Linux.libva ];
    };
    steam-hardware.enable = true;
  };
  services.udev.extraRules = ''
    # Disable DS4 touchpad acting as mouse
    # USB
    ATTRS{name}=="Sony Interactive Entertainment Wireless Controller Touchpad", ENV{LIBINPUT_IGNORE_DEVICE}="1"
    # Bluetooth
    ATTRS{name}=="Wireless Controller Touchpad", ENV{LIBINPUT_IGNORE_DEVICE}="1"
  '';
  security.rtkit.enable = true;
}
