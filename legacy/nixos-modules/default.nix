{
  pkgs,
  inputs,
  lib,
  ...
}:
let
  inherit (inputs) nixpkgs lix-module agenix;
in
{
  imports = [
    agenix.nixosModules.default
    lix-module.nixosModules.default
    ./adb.nix
    ./audio.nix
    ./backups
    ./extra_security
    ./flatpak.nix
    ./fonts.nix
    ./gaming.nix
    ./tailscale.nix
  ];
  boot.kernelPackages = pkgs.linuxPackages_latest;
  environment.systemPackages = [ agenix.packages.x86_64-linux.default ];
  nix = {
    registry.nixpkgs.flake = nixpkgs;
    settings = {
      auto-optimise-store = true;
      experimental-features = [
        "nix-command"
        "flakes"
        "repl-flake"
        "pipe-operator"
      ];
      system-features = [
        "big-parallel"
        "kvm"
        "nixos-test"
        "recursive-nix"
      ];
      nix-path = lib.mkForce "nixpkgs=${nixpkgs}";
      substituters = [
        "https://cache.nixos.org"
        "https://cache.lix.systems"
      ];
      trusted-users = [ "@wheel" ];
      trusted-public-keys = [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "cache.lix.systems:aBnZUw8zA7H35Cz2RyKFVs3H4PlGTLawyY5KRbvJR8o="
      ];
      use-xdg-base-directories = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
      persistent = true;
    };
  };
  services = {
    udisks2.enable = true;
  };

  users.users.kiri = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  };

  nixpkgs.config.allowUnfreePredicate =
    pkg:
    builtins.elem (lib.getName pkg) [
      "steam"
      "steam-run"
      "steam-unwrapped"
    ];
}
