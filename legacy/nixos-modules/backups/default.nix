{ config, ... }:
let
  homeDir = "/home/kiri/";
  common-excludes = [
    # Largest cache dirs
    ".cache"
    ".config"
    ".container-diff"
    ".npm"
    "Cache"
    "cache2" # firefox
    # Work related dirs
    ".direnv"
    ".local/share/Steam"
    ".m2"
    ".steam"
    ".stfolder*"
    ".stversions"
    ".thumbnails"
    ".tox"
    ".var"
    ".venv"
    "_build"
    "battlenet"
    "bower_components"
    "epic"
    "legendary"
    "node_modules"
    "plain"
    "rocket-league"
    "rocketleague"
    "torrented"
    "unhidden"
    "venv"
    ".plain"
  ];
  common-includes = map (dir: homeDir + dir) [
    "Desktop/"
    "Documents/"
    "Downloads/"
    "Games/"
    "Music/"
    "Pictures/"
    "Public/"
    "Sync/"
    "Templates/"
    "Videos/"
    "projects/personal/"
  ];
  default-restic = {
    exclude = common-excludes;
    initialize = false;
    timerConfig = {
      OnCalendar = "hourly";
      Persistent = true;
    };
    passwordFile = config.age.secrets."restic-snowfort".path;
    paths = common-includes;
    pruneOpts = [
      "--keep-within=1d"
      "--keep-daily 7"
      "--keep-weekly 4"
      "--keep-monthly 12"
      "--keep-yearly 5"
    ];
  };
in
{
  age.secrets = {
    "backblaze.env".file = ../../secrets/backblaze-snowfort.age;
    "restic-snowfort".file = ../../secrets/restic-snowfort.age;
  };
  services.restic.backups = {
    localSnowfort = default-restic // {
      repository = "/run/media/kiri/backup/restic/";
    };
    remoteSnowfort = default-restic // {
      repository = "b2:Snowfort";
      environmentFile = config.age.secrets."backblaze.env".path;
    };
  };
}
