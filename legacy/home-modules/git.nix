attributes = [
  "*.pdf diff=pdf"
  "*.md diff=md"
];
extraConfig = {
  branch.sort = "-committerdate";
  column.ui = "auto";
  commit.verbose = true;
  diff = {
    tool = "difftastic";
    algorithm = "histogram";
    sopsdiffer.textconv = "sops -d";
  };
  difftool = {
    prompt = false;
    "difftastic".cmd = ''difft "$LOCAL" "$REMOTE"'';
  };
  merge.tool = "nvimdiff";
  pager.difftool = true;
  push.autoSetupRemote = true;
  url."ssh://git@github.com/".insteadOf = "https://github.com/";
  rebase = {
    autostash = true;
    autosquash = true;
  };
  rerere.enabled = true;
};
ignores = [
  "*~"
  "*.swp"
  ".direnv"
  ".DS_Store"
];
