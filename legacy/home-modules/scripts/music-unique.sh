# TheFrenchGhosty's Ultimate YouTube-DL Scripts Collection: The ultimate collection of scripts for YouTube-DL
# https://github.com/TheFrenchGhosty/TheFrenchGhostys-Ultimate-YouTube-DL-Scripts-Collection
# https://github.com/TheFrenchGhosty
#
# Version: 3.3.0
#

RED='\033[0;31m'
NC='\033[0m' # No Color

if [ ! -e "Source - Unique.txt" ]; then
	printf "%sFile 'Source - Unique.txt' not found. Exiting.%s\n" "${RED}" "${NC}"
	exit 1
fi

yt-dlp --format "(bestaudio[acodec^=opus]/bestaudio)/best" \
	--verbose \
	--force-ipv4 \
	--sleep-requests 1 \
	--sleep-interval 5 \
	--max-sleep-interval 30 \
	--ignore-errors \
	--no-continue \
	--no-overwrites \
	--download-archive archive.log \
	--add-metadata \
	--parse-metadata "%(title)s:%(meta_title)s" \
	--parse-metadata "%(uploader)s:%(meta_artist)s" \
	--write-description \
	--write-info-json \
	--write-annotations \
	--write-thumbnail \
	--embed-thumbnail \
	--extract-audio \
	--check-formats \
	--concurrent-fragments 3 \
	--match-filter "!is_live & !live" \
	--output "%(title)s - %(uploader)s - %(upload_date)s/%(title)s - %(uploader)s - %(upload_date)s [%(id)s].%(ext)s" \
	--throttled-rate 100K \
	--batch-file "Source - Unique.txt" 2>&1 | tee output.log
