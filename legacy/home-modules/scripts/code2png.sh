#!/bin/sh
output_file="$(mktemp -t code2png-XXXXXX.png)"
cleanup() {
  printf "🧹 \033[0;33mCleaning up...\033[0m🧹 \n" # Print cleanup message in yellow
  rm -rf "$output_file"
  printf "\033[0;33mTemporary directory %s deleted.\033[0m\n" "$output_file" # Print directory deleted message in yellow
}

# Trap EXIT signal to ensure cleanup is run on script exit
trap cleanup EXIT
echo "$output_file"
silicon --tab-width 2 "$1" --output "$output_file"
swappy -f "$output_file"
