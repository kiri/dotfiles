#!/bin/sh
js="$(mktemp -t elm-size-comparison-XXXXXX.js)"
opt="$(mktemp -t elm-size-comparison-XXXXXX.opt.js)"
min="$(mktemp -t elm-size-comparison-XXXXXX.min.js)"

cleanup() {
  rm -f "$js" "$opt" "$min"
  printf "\033[33mTemporary files deleted.\033[0m\n"
}

trap cleanup EXIT

elm make ./src/Main.elm --output="$js" "$@"
elm make ./src/Main.elm --optimize --output="$opt" "$@"

uglifyjs "$js" --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe" | uglifyjs --mangle --output "$min"

echo "Initial size: $(wc -c "$js") bytes  ($js)"
echo "Optimized size: $(wc -c "$opt") bytes  ($opt)"
echo "Minified size:$(wc -c <"$min") bytes  ($min)"
echo "Gzipped size: $(gzip -c <"$min" | wc -c) bytes"
