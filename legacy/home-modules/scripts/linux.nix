{
  pkgs,
  lib,
  config,
  ...
}:
let
  dateSecond = "$(date +%s)";
  watchlistDir = "${config.xdg.userDirs.videos}/watchlist";
  inherit (lib) fileContents;
  inherit (pkgs) writeShellApplication;
  backupIfDuplicate =
    ext: # bash
    ''
      if [ "''${ext}" = "${ext}" ]; then
        bak="''${file}.bak"
        mv "''${file}" "''${bak}"
        file="''${file}.bak"
      fi
    '';
  process-inputs = # bash
    ''
      [ $# -eq 0 ] && notify-send "No arguments provided. Exitting..." && exit 1
      file="$(realpath -- "''${1}")"
      ext=''${file##*.}
      base=''${file%.*}
      directory=''${file%/*}
      export file ext base directory
    '';
  writeShellApplicationFromFile =
    {
      name,
      runtimeInputs ? [ ],
    }:
    writeShellApplication {
      inherit name runtimeInputs;
      text = fileContents ./${name}.sh;
    };
in
{
  options.linuxScripts.enable = lib.mkEnableOption "Enables linux scripts module";
  config = lib.mkIf config.linuxScripts.enable {
    home.packages = [
      (writeShellApplicationFromFile {
        name = "2opus";
        runtimeInputs = [
          pkgs.ffmpeg
          pkgs.mediainfo
        ];
      })
      (writeShellApplication {
        name = "2pdf";
        runtimeInputs = [ pkgs.unoconv ];
        text = ''
          ${process-inputs}
          case "$ext" in
            odt | docx | doc ) unoconv -f pdf "$file" ;;
            *) echo "I can't handle that format yet!" ;;
          esac
        '';
      })
      (writeShellApplicationFromFile {
        name = "code2png";
        runtimeInputs = with pkgs; [
          silicon
          swappy
        ];
      })
      (writeShellApplication {
        name = "download-media";
        runtimeInputs = [ pkgs.yt-dlp ];
        text = ''
          ${process-inputs}
          setsid yt-dlp --sponsorblock-mark all \
          --embed-subs --embed-metadata \
          -o "%(title)s-[%(id)s].%(ext)s" "$1" >>/dev/null &
        '';
      })
      (writeShellApplicationFromFile {
        name = "elm-size-comparison";
        runtimeInputs = with pkgs; [
          elmPackages.elm
          uglify-js
        ];
      })
      (writeShellApplicationFromFile {
        name = "error";
        runtimeInputs = [ pkgs.libnotify ];
      })
      (writeShellApplication {
        name = "feed-subscribe";
        text = ''
          if [ $# -eq 0 ]; then
            url=""
          else
            url="''${1}"
          fi
          xdg-open "https://reader.miniflux.app/bookmarklet?uri=''${url}"
        '';
      })
      (pkgs.writers.writeDashBin "tstenv" ''
        # Check if a command is provided
        if [ -z "$1" ]; then
        printf "\033[0;33mUsage: $0 <command>\033[0m\n"  # Print usage in yellow
        exit 1
        fi
        # Create a temporary directory and store its path
        temp_dir=$(mktemp -d)

        # Function to clean up the temporary directory
        cleanup() {
        printf "🧹 \033[0;33mCleaning up...\033[0m🧹 \n"  # Print cleanup message in yellow
        rm -rf "$temp_dir"
        printf "\033[0;33mTemporary directory $temp_dir deleted.\033[0m\n"  # Print directory deleted message in yellow
        }

        # Trap EXIT signal to ensure cleanup is run on script exit
        trap cleanup EXIT

        # Switch to the temporary directory
        cd "$temp_dir" || exit

        # Inform the user
        printf "\033[0;33mSwitched to temporary directory: $temp_dir\033[0m\n"  # Print switched message in yellow

        # Run the command passed as the first argument
        "$@"
      '')
      (pkgs.writers.writeRustBin "fabdirs" { } ''
        use std::fs;
        use std::io;

        fn main() -> io::Result<()> {
          [ "01-enero", "02-febrero", "03-marzo", "04-abril",
            "05-mayo", "06-junio", "07-julio", "08-agosto",
            "09-setiembre", "10-octubre", "11-noviembre", "12-diciembre" ]
            .iter().for_each(|dir| {
              match fs::create_dir(dir) {
                Ok(_) => println!("Directory {} created successfully.", dir),
                Err(e) => println!("Failed to create directory '{}': {}", dir, e),
              }});
          Ok(())
        }
      '')
      (writeShellApplicationFromFile { name = "git-remove-merged-branches"; })
      (writeShellApplicationFromFile {
        name = "music-unique";
        runtimeInputs = [ pkgs.yt-dlp ];
      })
      (writeShellApplicationFromFile {
        name = "videos-playlist-no-comments";
        runtimeInputs = [ pkgs.yt-dlp ];
      })
      (writeShellApplicationFromFile {
        name = "videos-unique";
        runtimeInputs = [ pkgs.yt-dlp ];
      })
      (writeShellApplication {
        name = "optisize";
        runtimeInputs = with pkgs; [
          file
          libjxl
          libwebp
          mediainfo
          handbrake
        ];
        text = ''
          blue='\033[1;34m INFO: '
          red='\033[1;31m ERROR: '
          reset='\033[0m'

          re-encode-video() {
            ${backupIfDuplicate "mkv"}
            temp_out="$(mktemp -t optisize-XXXXXXX.mkv)"
            HandBrakeCLI --preset "''${preset}" -i "''${file}" -o "''${temp_out}"
            mv "''${temp_out}" "''${base}.mkv"
          }

          video-optimize() {
            info="$(mediainfo "''${file}")"
            case "''${info}" in
              *"VP8"* | *"VP9"* )
                echo -e "''${blue}Old codecs detected, converting to AV1...''${reset}"
                export preset="AV1 MKV 2160p60 4K"
                re-encode-video
                ;;
              *"AVC"*)
                echo -e "''${blue}AVC detected, converting to HEVC...''${reset}"
                export preset="H.265 MKV 2160p60 4K"
                re-encode-video
                ;;
              *"HEVC"* | *"AV1"* ) echo "File already optimized." ;;
              *)
                echo -e "''${red}I don't know if I can optimize this file...''${reset}"
                exit 1
                ;;
            esac
          }

          ${process-inputs}
          mimetype="$(file --mime-type --brief "''${file}")"
          case "''${mimetype}" in
            image/jpeg)
              printf "%sConverting JPEG to JXL losslessly%s\n" "$blue" "$reset"
              cjxl --distance=0 "''${file}" "''${base}.jxl";;
            image/png)
              printf "%sConverting PNG to WEBP losslessly%s\n" "$blue" "$reset"
              cwebp -lossless "''${file}" -o "''${base}.webp"
              ;;
            "video/"*)
              video-optimize ;;
          * )
            echo -e "''${red}I don't know how to handle that file''${reset}"
            exit 1
            ;;
          esac
        '';
      })
      (writeShellApplication {
        name = "comparesize";
        text = ''
          # Check if the required commands are available
          die() {
            printf "%s\n" "$1" >&2
            exit 1
          }

          command -v printf >/dev/null 2>&1 || die "printf command is required."

          # Function to compare the sizes of two files
          compare_files() {
            jpg_file="$1"
            base="''${jpg_file%.*}"
            jxl_file="$base.jxl"

            if [ ! -f "$jpg_file" ]; then
              printf "Error: %s does not exist.\n" "$jpg_file" >&2
              return 1
            fi

            if [ ! -f "$jxl_file" ]; then
              printf "Error: %s does not exist.\n" "$jxl_file" >&2
              return 1
            fi

            jpg_size=$(stat -c %s "$jpg_file")
            jxl_size=$(stat -c %s "$jxl_file")

            if [ "$jxl_size" -lt "$jpg_size" ]; then
              printf "\033[32m%s is smaller (%d bytes vs %d bytes).\033[0m\n" "$jxl_file" "$jxl_size" "$jpg_size"
            else
              printf "\033[31m%s is smaller (%d bytes vs %d bytes).\033[0m\n" "$jpg_file" "$jpg_size" "$jxl_size"
            fi
          }

          # Main logic
          if [ "$#" -lt 1 ]; then
            printf "Usage: %s <basename> [<basename> ...]\n" "$0" >&2
            exit 1
          fi

          for base in "$@"; do
            compare_files "$base"
          done
        '';
      })
      (writeShellApplication {
        name = "mirror-screen";
        runtimeInputs = [ pkgs.scrcpy ];
        text = "scrcpy --stay-awake --disable-screensaver --turn-screen-off";
      })
      (writeShellApplication {
        name = "search";
        text = fileContents ./search.bash;
      })
      (writeShellApplication {
        name = "watchlist";
        runtimeInputs = [ pkgs.yt-dlp ];
        text = ''
          [ $# -eq 0 ] && setsid umpv "${watchlistDir}"
          path="''${1%%:*}"
          case "''${path}" in
            http|https) setsid yt-dlp \
              --sponsorblock-mark all\
              --embed-subs\
              --embed-metadata\
              -o "${watchlistDir}/${dateSecond}-%(title)s-[%(id)s].%(ext)s"\
            "''${1}" >>/dev/null & ;;
            *) setsid mv "''${1}" "${watchlistDir}/${dateSecond}-''${1}" ;;
          esac
        '';
      })
      (writeShellApplicationFromFile {
        name = "xdg-open";
        runtimeInputs = with pkgs; [
          file
          imv
          lagrange
          zathura
          libnotify
          xdg-utils
        ];
      })
    ];
  };
}
