engines=(
  "github.com/search?q="
  "flathub.org/apps/search?q="
  "nixos.wiki/index.php?search="
  "search.nixos.org/packages?channel=unstable&query="
  "search.nixos.org/options?channel=unstable&query="
  "protondb.com/search?q="
  "youtube.com/results?search_query="
  "crates.io/search?q="
  "noogle.dev/q?term="
)

selected_url="$(printf "%s\n" "${engines[@]}" | wofi --show=dmenu --prompt='Choose URL')"

if [ -z "${selected_url}" ]; then
  # User canceled, exit script
  exit 1
fi

input="$(wofi --show=dmenu --prompt='Enter Text')"

if [ -z "${input}" ]; then
  # User canceled, exit script
  exit 1
fi

# Open the selected URL in the default web browser
firefox -P default "${selected_url}${input}"
