{ pkgs, ... }:
let
  inherit (pkgs) writeShellApplication;
in
{
  imports = [ ./linux.nix ];
  linuxScripts.enable = pkgs.stdenv.isLinux;
  home.packages = [
    (writeShellApplication {
      name = "show-nix-store-path";
      text = ''realpath "$(command -v "''${1}")"'';
    })
    (writeShellApplication {
      name = "show-script";
      runtimeInputs = [ pkgs.bat ];
      text = ''bat "$(command -v "''${1}")"'';
    })
  ];
}
